﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExpresionesLambda
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello Lambda!");

            //FUNC
            Func<int, int> multiplicacion = (valor) => valor * 2;
            int resultadoMultiplicacion = multiplicacion(2);
            Console.WriteLine(resultadoMultiplicacion.ToString());


            Func<int, int, int> suma = (sumador, sumando) => sumador + sumando;
            int resultadoSuma = suma(2,3);
            Console.WriteLine(resultadoSuma.ToString());


            Func<int, int, int> comparador = (sumador, sumando) =>
            {
                if (sumador > sumando)
                {
                    return sumador;
                }
                else
                {
                    return sumando;
                }
            };
            int resultadoComparacion = comparador(2, 3);
            Console.WriteLine(resultadoSuma.ToString());


            List<int> Numeros = new List<int> {2,5,1,8,54,9,10 };
            Func<int, bool> evaluarPares = (numero) => numero % 2 == 0;
            //var pares = Numeros.Where(evaluarPares);
            var pares = Numeros.Where((numero) => numero % 2 == 0);

            //ACTION
            Action<int> imprimir = (numero) => Console.WriteLine(numero);
            imprimir(10);


            Func<int, Func<int, int>, int> funcionDentroFuncion = (numero, funcion) =>
            {
                if (numero == 100)
                {
                    return funcion(10000);
                }
                else
                {
                    return 200000;
                }
            };

            var resultado = funcionDentroFuncion(100, n => n + 1);
            imprimir(resultado);


            //PREDICATE
            Predicate<string> noNull = (valor) => valor != null;
            noNull("hola");



            Predicate<Usuario>[] validations =
            {
                beer => beer.Nombres != null,
                beer => beer.Apellidos !=null,
                beer => beer.Nombres.Count()>5 
            };
            Usuario user = new Usuario {Nombres="Edgardo", Apellidos="Panchana" };

            bool Validador(Usuario usuario, params Predicate<Usuario>[] validaciones) =>
                validaciones.ToList().Where(a =>
                {
                    return !a(usuario);
                }).Count()==0;

            Console.WriteLine(Validador(user, validations));

            //LINQ
            //SELECT --> MAP
            /*var numbers = [3,2,5,2,4,1,8,9 ];
             *var resultadoNumbers = numbers.map(x => x * 2);
             *console.log(resultadoNumbers);
             */

            var numbers = new int[] {2,5,2,4,8,9 };
            var resultadoSelect = numbers.Select(x => x * 2);

            //WHERE --> FILTER
            /*var numbers = [3,2,5,2,4,1,8,9 ];
             *var resultadoNumbers = numbers.filter(x => x > 5);
             *console.log(resultadoNumbers);
             */

            var resultadoWhere = numbers.Where(x => x > 5);

            //AGGREGATE --> REDUCE
            /*var numbers = [3,2,5,2,4,1,8,9 ];
             *var resultadoNumbers = numbers.reduce((sumador,item) => sumador + elemento);
             *console.log(resultadoNumbers);
             */

            var resultadoAggregate = numbers.Aggregate(0,(sumador,item) => sumador + item);
            Console.WriteLine(resultadoAggregate);

        }

        class Usuario
        {
            public string Nombres { get; set; }
            public string Apellidos { get; set; }

        }
    }
}
